from flask import Flask, render_template, Response
from camera import VideoCamera

app = Flask(__name__)


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html') 


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(VideoCamera()), mimetype='multipart/x-mixed-replace; boundary=frame')
                 
if __name__ == "__main__": 
    app.run(debug=True) 

# terminal output
# 127.0.0.1 - - [08/Jun/2021 21:42:16] "GET / HTTP/1.1" 200 -
# 127.0.0.1 - - [08/Jun/2021 21:42:17] "GET /video_feed HTTP/1.1" 200 -